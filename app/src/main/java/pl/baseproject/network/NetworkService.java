package pl.baseproject.network;

import pl.baseproject.model.QuestionOutput;
import pl.baseproject.model.SearchOutput;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface NetworkService {
    @GET("search?order=desc&sort=activity&site=stackoverflow")
    Observable<SearchOutput> getSearchResults(@Query("intitle") String intitle, @Query("page") int page, @Query("pagesize") int pagesize);

    @GET("questions/{id}?order=desc&sort=activity&site=stackoverflow&filter=withbody")
    Observable<QuestionOutput> getQuestionDetails(@Path("id") int id);

}