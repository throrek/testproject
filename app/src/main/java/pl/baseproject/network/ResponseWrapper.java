package pl.baseproject.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static pl.baseproject.network.ResponseWrapper.Status.ERROR;
import static pl.baseproject.network.ResponseWrapper.Status.LOADING;
import static pl.baseproject.network.ResponseWrapper.Status.SUCCESS;

public class ResponseWrapper<T> {
    @NonNull
    private final Status status;

    @Nullable
    private final T data;

    @Nullable
    private final Throwable throwable;

    private ResponseWrapper(@NonNull Status status, @Nullable T data, @Nullable Throwable throwable) {
        this.status = status;
        this.data = data;
        this.throwable = throwable;
    }

    public static <T> ResponseWrapper<T> success(@NonNull T data) {
        return new ResponseWrapper<>(SUCCESS, data, null);
    }

    public static <T> ResponseWrapper<T> error(Throwable throwable, @Nullable T data) {
        return new ResponseWrapper<>(ERROR, data, throwable);
    }

    public static <T> ResponseWrapper<T> loading(@Nullable T data) {
        return new ResponseWrapper<>(LOADING, data, null);
    }

    public boolean isLoading() {
        return LOADING.equals(status);
    }

    @Nullable
    public T getData() {
        return data;
    }

    @NonNull
    public Status getStatus() {
        return status;
    }

    public String getErrorMessage() {
        return throwable == null ? "" : throwable.getMessage();
    }

    public enum Status {
        SUCCESS, ERROR, LOADING
    }
}
