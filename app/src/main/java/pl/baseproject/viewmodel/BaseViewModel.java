package pl.baseproject.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import pl.baseproject.BaseApplication;
import pl.baseproject.network.NetworkService;
import rx.subscriptions.CompositeSubscription;

public abstract class BaseViewModel extends AndroidViewModel {
    @Inject
    NetworkService networkService;

    CompositeSubscription compositeSubscription = new CompositeSubscription();

    public BaseViewModel(@NonNull Application application) {
        super(application);
        ((BaseApplication) application).getMainComponent().inject(this);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeSubscription.unsubscribe();
    }
}
