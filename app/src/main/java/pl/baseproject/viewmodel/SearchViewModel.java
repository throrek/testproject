package pl.baseproject.viewmodel;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import pl.baseproject.model.SearchOutput;
import pl.baseproject.network.ResponseWrapper;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SearchViewModel extends BaseViewModel {
    private static final int PAGE_SIZE = 40;

    // MutableLiveData<NetworkResponse<List<SearchOutput.Items>>> I know it looks like a monster...
    private MutableLiveData<ResponseWrapper<List<SearchOutput.Items>>> result = new MutableLiveData<>();

    private List<SearchOutput.Items> items = new ArrayList<>();

    private SearchOutput lastSearchOutput;
    private String searchText;
    private int pageNumber;

    public SearchViewModel(@NonNull Application application) {
        super(application);
        result.setValue(ResponseWrapper.success(items));
    }

    public void getSearchQuestions(String searchText) {
        if (TextUtils.isEmpty(searchText)) {
            clearValues();
        } else {
            if (!searchText.equals(this.searchText)) {
                clearValues();
            }
            this.searchText = searchText;

            if ((lastSearchOutput != null && !lastSearchOutput.getHasMore()) || (result.getValue() != null && result.getValue().isLoading())) {
                return;
            }
            result.setValue(ResponseWrapper.loading(items));
            // Network operations could be in separate repository class
            compositeSubscription.add(networkService.getSearchResults(searchText, pageNumber, PAGE_SIZE)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<SearchOutput>() {
                        @Override
                        public void call(SearchOutput searchOutput) {
                            items.addAll(searchOutput.getItems());
                            lastSearchOutput = searchOutput;
                            pageNumber++;

                            result.setValue(ResponseWrapper.success(items));
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            result.setValue(ResponseWrapper.error(throwable, items));
                        }
                    }));
        }
    }

    public MutableLiveData<ResponseWrapper<List<SearchOutput.Items>>> getResult() {
        return result;
    }

    private void clearValues() {
        items.clear();
        pageNumber = 1;
        lastSearchOutput = null;
        result.setValue(ResponseWrapper.success(items));
    }
}
