package pl.baseproject.viewmodel;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import pl.baseproject.model.QuestionOutput;
import pl.baseproject.network.ResponseWrapper;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class QuestionViewModel extends BaseViewModel {
    private MutableLiveData<ResponseWrapper<QuestionOutput>> questionDetails;

    public QuestionViewModel(@NonNull Application application) {
        super(application);
    }

    private void getData(int questionId) {
        questionDetails.setValue(ResponseWrapper.<QuestionOutput>loading(null));
        compositeSubscription.add(networkService.getQuestionDetails(questionId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<QuestionOutput>() {
                    @Override
                    public void call(QuestionOutput questionOutput) {
                        questionDetails.setValue(ResponseWrapper.success(questionOutput));
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        questionDetails.setValue(ResponseWrapper.<QuestionOutput>error(throwable, null));
                    }
                }));
    }

    public MutableLiveData<ResponseWrapper<QuestionOutput>> getQuestionDetails(int questionId) {
        if (questionDetails == null) {
            questionDetails = new MutableLiveData<>();
            getData(questionId);
        }
        return questionDetails;
    }
}
