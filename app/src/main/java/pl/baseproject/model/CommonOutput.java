package pl.baseproject.model;

import com.google.gson.annotations.SerializedName;

public class CommonOutput {
    @SerializedName("has_more")
    private boolean has_more;
    @SerializedName("quota_max")
    private int quota_max;
    @SerializedName("quota_remaining")
    private int quota_remaining;

    public boolean getHasMore() {
        return has_more;
    }

    public int getQuotaMax() {
        return quota_max;
    }

    public int getQuotaRemaining() {
        return quota_remaining;
    }
}
