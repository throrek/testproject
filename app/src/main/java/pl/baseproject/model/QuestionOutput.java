package pl.baseproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionOutput extends CommonOutput {
    @SerializedName("items")
    private List<Items> items;

    public List<Items> getItems() {
        return items;
    }

    public static class Items {
        @SerializedName("tags")
        public List<String> tags;
        @SerializedName("owner")
        public Owner owner;
        @SerializedName("is_answered")
        public boolean is_answered;
        @SerializedName("view_count")
        public int view_count;
        @SerializedName("accepted_answer_id")
        public int accepted_answer_id;
        @SerializedName("answer_count")
        public int answer_count;
        @SerializedName("score")
        public int score;
        @SerializedName("last_activity_date")
        public int last_activity_date;
        @SerializedName("creation_date")
        public int creation_date;
        @SerializedName("question_id")
        public int question_id;
        @SerializedName("link")
        public String link;
        @SerializedName("title")
        public String title;
        @SerializedName("body")
        public String body;

        public List<String> getTags() {
            return tags;
        }

        public Owner getOwner() {
            return owner;
        }

        public boolean isIs_answered() {
            return is_answered;
        }

        public int getView_count() {
            return view_count;
        }

        public int getAccepted_answer_id() {
            return accepted_answer_id;
        }

        public int getAnswer_count() {
            return answer_count;
        }

        public int getScore() {
            return score;
        }

        public int getLast_activity_date() {
            return last_activity_date;
        }

        public int getCreation_date() {
            return creation_date;
        }

        public int getQuestion_id() {
            return question_id;
        }

        public String getLink() {
            return link;
        }

        public String getTitle() {
            return title;
        }

        public String getBody() {
            return body;
        }
    }
}
