package pl.baseproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchOutput extends CommonOutput {
    @SerializedName("items")
    private List<Items> items;

    public List<Items> getItems() {
        return items;
    }

    public static class Items {
        @SerializedName("tags")
        private List<String> tags;
        @SerializedName("owner")
        private Owner owner;
        @SerializedName("is_answered")
        private boolean is_answered;
        @SerializedName("view_count")
        private int view_count;
        @SerializedName("answer_count")
        private int answer_count;
        @SerializedName("score")
        private int score;
        @SerializedName("last_activity_date")
        private int last_activity_date;
        @SerializedName("creation_date")
        private int creation_date;
        @SerializedName("last_edit_date")
        private int last_edit_date;
        @SerializedName("question_id")
        private int question_id;
        @SerializedName("link")
        private String link;
        @SerializedName("title")
        private String title;

        public List<String> getTags() {
            return tags;
        }

        public Owner getOwner() {
            return owner;
        }

        public boolean getIsAnswered() {
            return is_answered;
        }

        public int getViewCount() {
            return view_count;
        }

        public int getAnswerCount() {
            return answer_count;
        }

        public int getScore() {
            return score;
        }

        public int getLastActivityDate() {
            return last_activity_date;
        }

        public int getCreationDate() {
            return creation_date;
        }

        public int getLastEditDate() {
            return last_edit_date;
        }

        public int getQuestionId() {
            return question_id;
        }

        public String getLink() {
            return link;
        }

        public String getTitle() {
            return title;
        }
    }
}
