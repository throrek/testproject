package pl.baseproject.model;

import com.google.gson.annotations.SerializedName;

public class Owner {
    @SerializedName("reputation")
    private int reputation;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("user_type")
    private String user_type;
    @SerializedName("profile_image")
    private String profile_image;
    @SerializedName("display_name")
    private String display_name;
    @SerializedName("link")
    private String link;

    public int getReputation() {
        return reputation;
    }

    public int getUserId() {
        return user_id;
    }

    public String getUserType() {
        return user_type;
    }

    public String getProfileImage() {
        return profile_image;
    }

    public String getDisplayName() {
        return display_name;
    }

    public String getLink() {
        return link;
    }
}