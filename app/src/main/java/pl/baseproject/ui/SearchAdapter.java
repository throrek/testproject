package pl.baseproject.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import pl.baseproject.R;
import pl.baseproject.model.SearchOutput;

import static butterknife.ButterKnife.bind;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchHolder> {

    private List<SearchOutput.Items> adapterData = new ArrayList<>();

    public void setData(List<SearchOutput.Items> adapterData) {
        this.adapterData = adapterData;
    }

    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SearchHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false));
    }

    @Override
    public void onBindViewHolder(final SearchHolder holder, int position) {
        final SearchOutput.Items item = adapterData.get(position);
        final Context context = holder.itemView.getContext();
        holder.ownerTv.setText(item.getOwner().getDisplayName());
        holder.titleTv.setText(item.getTitle());

        holder.ownerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBrowser(item.getOwner().getLink(), holder.itemView.getContext());
            }
        });

        holder.titleTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(QuestionDetailsActivity.getIntent(context, item.getQuestionId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return adapterData == null ? 0 : adapterData.size();
    }

    class SearchHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.search_item_owner)
        TextView ownerTv;

        @BindView(R.id.search_item_title)
        TextView titleTv;

        SearchHolder(View itemView) {
            super(itemView);
            bind(this, itemView);
        }
    }

    private void openBrowser(String url, Context context) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        if (browserIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(browserIntent);
        }
    }
}