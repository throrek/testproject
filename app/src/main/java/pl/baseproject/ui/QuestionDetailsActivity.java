package pl.baseproject.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import pl.baseproject.R;
import pl.baseproject.model.QuestionOutput;
import pl.baseproject.network.ResponseWrapper;
import pl.baseproject.viewmodel.QuestionViewModel;

import static butterknife.ButterKnife.bind;

public class QuestionDetailsActivity extends AppCompatActivity {
    private static final String EXTRA_QUESTION_ID = "extra_question_id";

    @BindView(R.id.search_progressbar)
    ProgressBar progressBar;

    @BindView(R.id.question_body_tv)
    TextView questionBodyTv;

    public static Intent getIntent(Context context, int questionId) {
        Intent intent = new Intent(context, QuestionDetailsActivity.class);
        intent.putExtra(EXTRA_QUESTION_ID, questionId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_details);
        bind(this);

        questionBodyTv.setMovementMethod(LinkMovementMethod.getInstance());

        int questionId = getIntent().getIntExtra(EXTRA_QUESTION_ID, -1);

        QuestionViewModel questionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);
        questionViewModel.getQuestionDetails(questionId).observe(this, new Observer<ResponseWrapper<QuestionOutput>>() {
            @Override
            public void onChanged(@Nullable ResponseWrapper<QuestionOutput> questionDetails) {
                if (questionDetails != null) {
                    switch (questionDetails.getStatus()) {
                        case SUCCESS:
                            progressBar.setVisibility(View.INVISIBLE);
                            if (questionDetails.getData() != null &&
                                    questionDetails.getData().getItems() != null &&
                                    !questionDetails.getData().getItems().isEmpty()) {
                                questionBodyTv.setText(Html.fromHtml(questionDetails.getData().getItems().get(0).getBody()));
                            }
                            break;
                        case ERROR:
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(QuestionDetailsActivity.this, questionDetails.getErrorMessage(), Toast.LENGTH_LONG).show();
                            break;
                        case LOADING:
                            progressBar.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }
        });
    }
}
