package pl.baseproject.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import pl.baseproject.R;
import pl.baseproject.model.SearchOutput;
import pl.baseproject.network.ResponseWrapper;
import pl.baseproject.viewmodel.SearchViewModel;

import static butterknife.ButterKnife.bind;

public class SearchActivity extends AppCompatActivity {
    private static final int LOAD_THRESHOLD = 15;

    @BindView(R.id.search_top_container)
    LinearLayout topContainer;

    @BindView(R.id.search_edit_text)
    EditText searchEt;

    @BindView(R.id.search_button)
    Button searchButton;

    @BindView(R.id.search_recycler_view)
    RecyclerView mainRecyclerView;

    @BindView(R.id.search_progressbar)
    ProgressBar progressBar;

    private SearchAdapter searchAdapter = new SearchAdapter();
    private SearchViewModel searchViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind(this);

        initViewModel();
        initView();
    }

    private void initViewModel() {
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        searchViewModel.getResult().observe(this, new Observer<ResponseWrapper<List<SearchOutput.Items>>>() {
            @Override
            public void onChanged(@Nullable ResponseWrapper<List<SearchOutput.Items>> listNetworkResponse) {
                if (listNetworkResponse != null) {
                    switch (listNetworkResponse.getStatus()) {
                        case SUCCESS:
                            progressBar.setVisibility(View.INVISIBLE);
                            searchAdapter.setData(listNetworkResponse.getData());
                            searchAdapter.notifyDataSetChanged();
                            break;
                        case LOADING:
                            progressBar.setVisibility(View.VISIBLE);
                            break;
                        case ERROR:
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(SearchActivity.this, listNetworkResponse.getErrorMessage(), Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            }
        });
    }

    private void initView() {
        ViewCompat.setElevation(topContainer, getResources().getDimensionPixelSize(R.dimen.shadow_height));

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mainRecyclerView.setLayoutManager(linearLayoutManager);
        mainRecyclerView.setAdapter(searchAdapter);
        mainRecyclerView.addItemDecoration(new DividerItemDecoration(this, linearLayoutManager.getOrientation()));

        mainRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (searchAdapter.getItemCount() - linearLayoutManager.findLastCompletelyVisibleItemPosition() < LOAD_THRESHOLD) {
                    searchForQuestion();
                }
            }
        });

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    hideKeyboard();
                    searchForQuestion();
                    return true;
                }
                return false;
            }
        });
    }

    private void searchForQuestion() {
        searchViewModel.getSearchQuestions(searchEt.getText().toString());
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(searchEt.getWindowToken(), 0);
        }
    }

    @OnClick(R.id.search_button)
    public void onSearchClick() {
        hideKeyboard();
        searchForQuestion();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainRecyclerView.clearOnScrollListeners();
    }
}
