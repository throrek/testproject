package pl.baseproject.dagger;

import javax.inject.Singleton;

import dagger.Component;
import pl.baseproject.dagger.modules.ApplicationModule;
import pl.baseproject.dagger.modules.NetworkModule;
import pl.baseproject.viewmodel.BaseViewModel;
import pl.baseproject.ui.SearchActivity;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface MainComponent {
    void inject(SearchActivity activity);
    void inject(BaseViewModel baseViewModel);
}