package pl.baseproject;

import android.app.Application;

import pl.baseproject.dagger.DaggerMainComponent;
import pl.baseproject.dagger.MainComponent;
import pl.baseproject.dagger.modules.ApplicationModule;
import pl.baseproject.dagger.modules.NetworkModule;

public class BaseApplication extends Application {
    private static final String BASE_URL = "https://api.stackexchange.com/2.2/";
    private MainComponent mainComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mainComponent = DaggerMainComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(BASE_URL))
                .build();
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }
}
